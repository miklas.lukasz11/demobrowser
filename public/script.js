function getXHR(url, sync) {
    var xhr = new XMLHttpRequest();

    function handleEvent(e) {
        console.log(`${e.type.padEnd(16, ' ')} - readyState: ${xhr.readyState}, status: ${String(xhr.status).padStart(3, ' ')}, bytes: ${e.loaded}`);
    }

    function doLoop(e,uri) {
        for(var i=0;i<e;i++){
            getFetch_blob(uri)
        }
    }


    xhr.addEventListener('readystatechange', handleEvent);
    xhr.addEventListener('loadstart', handleEvent);
    xhr.addEventListener('load', handleEvent);
    xhr.addEventListener('loadend', handleEvent);
    xhr.addEventListener('progress', handleEvent);
    xhr.addEventListener('error', handleEvent);
    xhr.addEventListener('abort', handleEvent);
    xhr.addEventListener('timeout', handleEvent);

    xhr.open('GET', url, !sync);

    try {
        xhr.send();
        // xhr.abort();

        console.log('This is done after, should not be part of error recording for above.');

    } catch (err) {
        console.log('xhr.send():', err);

        throw err;
    }

    return xhr;
}

function getXHRSync(uri) {
    return getXHR.call(this, uri, true);
}

function getFetch(uri) {
    fetch(uri).then(
        // (r) => r.text().then(r => console.log('fetch resp:', r)),
        (r) => console.log('fetch resp:', r, r.text().then((r) => console.log(r))),
        (e) => console.dir(e, {depth: null})
    );

    console.log('This is done after, should not be part of error recording for above.');
}

function getFetch_arrayBuffer(uri) {
    fetch(uri).then(
        (r) => r.arrayBuffer().then(
            (r) => console.log('fetch resp:', r),
            (e) => console.dir(e, {depth: null})
        ),
        (e) => console.dir(e, {depth: null})
    );

    console.log('This is done after, should not be part of error recording for above.');
}

function getFetch_blob(uri) {
    fetch(uri).then(
        (r) => r.blob().then(
            (r) => console.log('fetch resp:', r),
            (e) => console.dir(e, {depth: null})
        ),
        (e) => console.dir(e, {depth: null})
    );

    console.log('This is done after, should not be part of error recording for above.');
}

function getFetch_text(uri) {
    fetch(uri).then(
        (r) => r.text().then(
            (r) => console.log('fetch resp:', r),
            (e) => console.dir(e, {depth: null})
        ),
        (e) => console.dir(e, {depth: null})
    );

    console.log('This is done after, should not be part of error recording for above.');
}

function getFetch_getReader(uri) {
    fetch(uri).then(
        (r) => {
            const reader = r.body.getReader();

            reader.read().then(e => console.log(r));
            reader.read().then(e => console.log(r));
            reader.read().then(e => console.log(r));
        },
        (e) => console.dir(e, {depth: null})
    );

    console.log('This is done after, should not be part of error recording for above.');
}

function burn() {
    let e;

    e = document.createElement('img');
    document.body.querySelector('#Div1').appendChild(e);
    e.setAttribute('id', 'Img1');
    e.setAttribute('src', 'notthere.jpg');

    e = document.createElement('script');
    e.setAttribute('id', 'Script1');
    e.setAttribute('class', 'a b   c');
    e.setAttribute('src', 'notthere.js');
    document.body.querySelector('#Div1').appendChild(e);

}
