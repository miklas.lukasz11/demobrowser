FROM node:14

WORKDIR /starterDemo

COPY package.json /starterDemo/package.json

RUN npm install 

RUN npm config set @revdebug:registry https://nexus.revdebug.com/repository/npm/

RUN npm install grpc

RUN npm install @revdebug/revdebug

COPY . /starterDemo

RUN npx revd

CMD ["node","app.js"]

EXPOSE 6018
