#!/usr/bin/env node

const port = 6018

const fs       = require('fs');
const express  = require('express');

const app      = express();

app.use(express.static('public'));

app.get('/good', function(req, res) {
    console.log('GET /')
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('Testing...');
    setTimeout(() => {
        res.end('Good! XD');
    }, 250);
})

app.get('/500', function(req, res) {
    throw new Error('Bad! :_(');
})

app.listen(port);

console.log(`Listening at http://localhost:${port}`);

require('child_process').exec(
    (process.platform === 'darwin' ? 'open' : process.platform === 'win32' ? 'start' : 'xdg-open') +
    ` http://localhost:${port}/`);
